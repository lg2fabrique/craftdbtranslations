<?php
/**
 * Translations Database plugin for Craft CMS 3.x
 *
 * Translate string from database key.
 *
 * @link      https://github.com/robin-gauthier
 * @copyright Copyright (c) 2019 Robin Gauthier
 */

namespace lg2\craftdbtranslations\records;

use lg2\craftdbtranslations\TranslationsDatabase;

use Craft;
use craft\db\ActiveRecord;

/**
 * @author    Robin Gauthier
 * @package   TranslationsDatabase
 * @since     1.0.0
 * @property string $translation_key
 * @property string $value
 * @property string $lang
 */
class TranslationsRecord extends ActiveRecord
{
    // Public Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%translations_database}}';
    }
}
